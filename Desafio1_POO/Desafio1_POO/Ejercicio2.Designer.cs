﻿
namespace Desafio1_POO
{
    partial class frmEjercicio2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtIngresoVotos = new System.Windows.Forms.TextBox();
            this.listVotos = new System.Windows.Forms.ListBox();
            this.lblCantidadVotos = new System.Windows.Forms.Label();
            this.grElecciones = new System.Windows.Forms.GroupBox();
            this.txtPorcentajeCandidato4 = new System.Windows.Forms.TextBox();
            this.txtPorcentajeCandidato3 = new System.Windows.Forms.TextBox();
            this.txtPorcentajeCandidato2 = new System.Windows.Forms.TextBox();
            this.txtPorcentajeCandidato1 = new System.Windows.Forms.TextBox();
            this.txtVotosCandidato4 = new System.Windows.Forms.TextBox();
            this.txtVotosCandidato3 = new System.Windows.Forms.TextBox();
            this.txtVotosCandidato2 = new System.Windows.Forms.TextBox();
            this.txtVotosCandidato1 = new System.Windows.Forms.TextBox();
            this.lblPorcentajeCandidato4 = new System.Windows.Forms.Label();
            this.lblPorcentajeCandidato3 = new System.Windows.Forms.Label();
            this.lblPorcentajeCandidato2 = new System.Windows.Forms.Label();
            this.lblPorcentajeCandidato1 = new System.Windows.Forms.Label();
            this.lblVotosCandidato4 = new System.Windows.Forms.Label();
            this.lblVotosCandidato3 = new System.Windows.Forms.Label();
            this.lblVotosCandidato2 = new System.Windows.Forms.Label();
            this.lblVotosCandidato1 = new System.Windows.Forms.Label();
            this.lblCandidato4 = new System.Windows.Forms.Label();
            this.lblCandidato3 = new System.Windows.Forms.Label();
            this.lblCandidato2 = new System.Windows.Forms.Label();
            this.lblCandidato1 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnConteo = new System.Windows.Forms.Button();
            this.lblTitulo = new System.Windows.Forms.Label();
            this.grElecciones.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtIngresoVotos
            // 
            this.txtIngresoVotos.Location = new System.Drawing.Point(248, 105);
            this.txtIngresoVotos.Name = "txtIngresoVotos";
            this.txtIngresoVotos.Size = new System.Drawing.Size(253, 22);
            this.txtIngresoVotos.TabIndex = 0;
            this.txtIngresoVotos.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtIngresoVotos_KeyPress);
            // 
            // listVotos
            // 
            this.listVotos.FormattingEnabled = true;
            this.listVotos.ItemHeight = 16;
            this.listVotos.Location = new System.Drawing.Point(51, 226);
            this.listVotos.Name = "listVotos";
            this.listVotos.Size = new System.Drawing.Size(167, 228);
            this.listVotos.TabIndex = 1;
            // 
            // lblCantidadVotos
            // 
            this.lblCantidadVotos.AutoSize = true;
            this.lblCantidadVotos.Location = new System.Drawing.Point(48, 105);
            this.lblCantidadVotos.Name = "lblCantidadVotos";
            this.lblCantidadVotos.Size = new System.Drawing.Size(180, 17);
            this.lblCantidadVotos.TabIndex = 2;
            this.lblCantidadVotos.Text = "Ingresar cantidad de votos ";
            // 
            // grElecciones
            // 
            this.grElecciones.Controls.Add(this.txtPorcentajeCandidato4);
            this.grElecciones.Controls.Add(this.txtPorcentajeCandidato3);
            this.grElecciones.Controls.Add(this.txtPorcentajeCandidato2);
            this.grElecciones.Controls.Add(this.txtPorcentajeCandidato1);
            this.grElecciones.Controls.Add(this.txtVotosCandidato4);
            this.grElecciones.Controls.Add(this.txtVotosCandidato3);
            this.grElecciones.Controls.Add(this.txtVotosCandidato2);
            this.grElecciones.Controls.Add(this.txtVotosCandidato1);
            this.grElecciones.Controls.Add(this.lblPorcentajeCandidato4);
            this.grElecciones.Controls.Add(this.lblPorcentajeCandidato3);
            this.grElecciones.Controls.Add(this.lblPorcentajeCandidato2);
            this.grElecciones.Controls.Add(this.lblPorcentajeCandidato1);
            this.grElecciones.Controls.Add(this.lblVotosCandidato4);
            this.grElecciones.Controls.Add(this.lblVotosCandidato3);
            this.grElecciones.Controls.Add(this.lblVotosCandidato2);
            this.grElecciones.Controls.Add(this.lblVotosCandidato1);
            this.grElecciones.Controls.Add(this.lblCandidato4);
            this.grElecciones.Controls.Add(this.lblCandidato3);
            this.grElecciones.Controls.Add(this.lblCandidato2);
            this.grElecciones.Controls.Add(this.lblCandidato1);
            this.grElecciones.Location = new System.Drawing.Point(248, 181);
            this.grElecciones.Name = "grElecciones";
            this.grElecciones.Size = new System.Drawing.Size(511, 276);
            this.grElecciones.TabIndex = 3;
            this.grElecciones.TabStop = false;
            this.grElecciones.Text = "Elecciones de candidatos";
            // 
            // txtPorcentajeCandidato4
            // 
            this.txtPorcentajeCandidato4.Location = new System.Drawing.Point(389, 213);
            this.txtPorcentajeCandidato4.Name = "txtPorcentajeCandidato4";
            this.txtPorcentajeCandidato4.ReadOnly = true;
            this.txtPorcentajeCandidato4.Size = new System.Drawing.Size(87, 22);
            this.txtPorcentajeCandidato4.TabIndex = 42;
            // 
            // txtPorcentajeCandidato3
            // 
            this.txtPorcentajeCandidato3.Location = new System.Drawing.Point(389, 154);
            this.txtPorcentajeCandidato3.Name = "txtPorcentajeCandidato3";
            this.txtPorcentajeCandidato3.ReadOnly = true;
            this.txtPorcentajeCandidato3.Size = new System.Drawing.Size(87, 22);
            this.txtPorcentajeCandidato3.TabIndex = 41;
            // 
            // txtPorcentajeCandidato2
            // 
            this.txtPorcentajeCandidato2.Location = new System.Drawing.Point(389, 98);
            this.txtPorcentajeCandidato2.Name = "txtPorcentajeCandidato2";
            this.txtPorcentajeCandidato2.ReadOnly = true;
            this.txtPorcentajeCandidato2.Size = new System.Drawing.Size(87, 22);
            this.txtPorcentajeCandidato2.TabIndex = 40;
            // 
            // txtPorcentajeCandidato1
            // 
            this.txtPorcentajeCandidato1.Location = new System.Drawing.Point(389, 47);
            this.txtPorcentajeCandidato1.Name = "txtPorcentajeCandidato1";
            this.txtPorcentajeCandidato1.ReadOnly = true;
            this.txtPorcentajeCandidato1.Size = new System.Drawing.Size(87, 22);
            this.txtPorcentajeCandidato1.TabIndex = 39;
            // 
            // txtVotosCandidato4
            // 
            this.txtVotosCandidato4.Location = new System.Drawing.Point(182, 213);
            this.txtVotosCandidato4.Name = "txtVotosCandidato4";
            this.txtVotosCandidato4.ReadOnly = true;
            this.txtVotosCandidato4.Size = new System.Drawing.Size(90, 22);
            this.txtVotosCandidato4.TabIndex = 38;
            // 
            // txtVotosCandidato3
            // 
            this.txtVotosCandidato3.Location = new System.Drawing.Point(182, 154);
            this.txtVotosCandidato3.Name = "txtVotosCandidato3";
            this.txtVotosCandidato3.ReadOnly = true;
            this.txtVotosCandidato3.Size = new System.Drawing.Size(90, 22);
            this.txtVotosCandidato3.TabIndex = 37;
            // 
            // txtVotosCandidato2
            // 
            this.txtVotosCandidato2.Location = new System.Drawing.Point(182, 98);
            this.txtVotosCandidato2.Name = "txtVotosCandidato2";
            this.txtVotosCandidato2.ReadOnly = true;
            this.txtVotosCandidato2.Size = new System.Drawing.Size(90, 22);
            this.txtVotosCandidato2.TabIndex = 36;
            // 
            // txtVotosCandidato1
            // 
            this.txtVotosCandidato1.Location = new System.Drawing.Point(182, 42);
            this.txtVotosCandidato1.Name = "txtVotosCandidato1";
            this.txtVotosCandidato1.ReadOnly = true;
            this.txtVotosCandidato1.Size = new System.Drawing.Size(90, 22);
            this.txtVotosCandidato1.TabIndex = 35;
            // 
            // lblPorcentajeCandidato4
            // 
            this.lblPorcentajeCandidato4.AutoSize = true;
            this.lblPorcentajeCandidato4.Location = new System.Drawing.Point(295, 216);
            this.lblPorcentajeCandidato4.Name = "lblPorcentajeCandidato4";
            this.lblPorcentajeCandidato4.Size = new System.Drawing.Size(84, 17);
            this.lblPorcentajeCandidato4.TabIndex = 34;
            this.lblPorcentajeCandidato4.Text = "Porcentaje: ";
            // 
            // lblPorcentajeCandidato3
            // 
            this.lblPorcentajeCandidato3.AutoSize = true;
            this.lblPorcentajeCandidato3.Location = new System.Drawing.Point(295, 154);
            this.lblPorcentajeCandidato3.Name = "lblPorcentajeCandidato3";
            this.lblPorcentajeCandidato3.Size = new System.Drawing.Size(84, 17);
            this.lblPorcentajeCandidato3.TabIndex = 33;
            this.lblPorcentajeCandidato3.Text = "Porcentaje: ";
            // 
            // lblPorcentajeCandidato2
            // 
            this.lblPorcentajeCandidato2.AutoSize = true;
            this.lblPorcentajeCandidato2.Location = new System.Drawing.Point(295, 103);
            this.lblPorcentajeCandidato2.Name = "lblPorcentajeCandidato2";
            this.lblPorcentajeCandidato2.Size = new System.Drawing.Size(84, 17);
            this.lblPorcentajeCandidato2.TabIndex = 32;
            this.lblPorcentajeCandidato2.Text = "Porcentaje: ";
            // 
            // lblPorcentajeCandidato1
            // 
            this.lblPorcentajeCandidato1.AutoSize = true;
            this.lblPorcentajeCandidato1.Location = new System.Drawing.Point(295, 47);
            this.lblPorcentajeCandidato1.Name = "lblPorcentajeCandidato1";
            this.lblPorcentajeCandidato1.Size = new System.Drawing.Size(84, 17);
            this.lblPorcentajeCandidato1.TabIndex = 31;
            this.lblPorcentajeCandidato1.Text = "Porcentaje: ";
            // 
            // lblVotosCandidato4
            // 
            this.lblVotosCandidato4.AutoSize = true;
            this.lblVotosCandidato4.Location = new System.Drawing.Point(130, 216);
            this.lblVotosCandidato4.Name = "lblVotosCandidato4";
            this.lblVotosCandidato4.Size = new System.Drawing.Size(44, 17);
            this.lblVotosCandidato4.TabIndex = 30;
            this.lblVotosCandidato4.Text = "Votos";
            // 
            // lblVotosCandidato3
            // 
            this.lblVotosCandidato3.AutoSize = true;
            this.lblVotosCandidato3.Location = new System.Drawing.Point(130, 154);
            this.lblVotosCandidato3.Name = "lblVotosCandidato3";
            this.lblVotosCandidato3.Size = new System.Drawing.Size(44, 17);
            this.lblVotosCandidato3.TabIndex = 29;
            this.lblVotosCandidato3.Text = "Votos";
            // 
            // lblVotosCandidato2
            // 
            this.lblVotosCandidato2.AutoSize = true;
            this.lblVotosCandidato2.Location = new System.Drawing.Point(130, 103);
            this.lblVotosCandidato2.Name = "lblVotosCandidato2";
            this.lblVotosCandidato2.Size = new System.Drawing.Size(44, 17);
            this.lblVotosCandidato2.TabIndex = 28;
            this.lblVotosCandidato2.Text = "Votos";
            // 
            // lblVotosCandidato1
            // 
            this.lblVotosCandidato1.AutoSize = true;
            this.lblVotosCandidato1.Location = new System.Drawing.Point(130, 47);
            this.lblVotosCandidato1.Name = "lblVotosCandidato1";
            this.lblVotosCandidato1.Size = new System.Drawing.Size(44, 17);
            this.lblVotosCandidato1.TabIndex = 27;
            this.lblVotosCandidato1.Text = "Votos";
            // 
            // lblCandidato4
            // 
            this.lblCandidato4.AutoSize = true;
            this.lblCandidato4.Location = new System.Drawing.Point(26, 216);
            this.lblCandidato4.Name = "lblCandidato4";
            this.lblCandidato4.Size = new System.Drawing.Size(88, 17);
            this.lblCandidato4.TabIndex = 26;
            this.lblCandidato4.Text = "Candidato 4:";
            // 
            // lblCandidato3
            // 
            this.lblCandidato3.AutoSize = true;
            this.lblCandidato3.Location = new System.Drawing.Point(26, 156);
            this.lblCandidato3.Name = "lblCandidato3";
            this.lblCandidato3.Size = new System.Drawing.Size(88, 17);
            this.lblCandidato3.TabIndex = 25;
            this.lblCandidato3.Text = "Candidato 3:";
            // 
            // lblCandidato2
            // 
            this.lblCandidato2.AutoSize = true;
            this.lblCandidato2.Location = new System.Drawing.Point(26, 105);
            this.lblCandidato2.Name = "lblCandidato2";
            this.lblCandidato2.Size = new System.Drawing.Size(88, 17);
            this.lblCandidato2.TabIndex = 24;
            this.lblCandidato2.Text = "Candidato 2:";
            // 
            // lblCandidato1
            // 
            this.lblCandidato1.AutoSize = true;
            this.lblCandidato1.Location = new System.Drawing.Point(26, 49);
            this.lblCandidato1.Name = "lblCandidato1";
            this.lblCandidato1.Size = new System.Drawing.Size(88, 17);
            this.lblCandidato1.TabIndex = 23;
            this.lblCandidato1.Text = "Candidato 1:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(48, 181);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 17);
            this.label1.TabIndex = 5;
            this.label1.Text = "Listado de votos";
            // 
            // btnConteo
            // 
            this.btnConteo.BackColor = System.Drawing.Color.Transparent;
            this.btnConteo.BackgroundImage = global::Desafio1_POO.Properties.Resources.lesson;
            this.btnConteo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnConteo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConteo.ForeColor = System.Drawing.Color.Transparent;
            this.btnConteo.Location = new System.Drawing.Point(546, 65);
            this.btnConteo.Name = "btnConteo";
            this.btnConteo.Size = new System.Drawing.Size(178, 103);
            this.btnConteo.TabIndex = 4;
            this.btnConteo.UseVisualStyleBackColor = false;
            this.btnConteo.Click += new System.EventHandler(this.btnConteo_Click);
            // 
            // lblTitulo
            // 
            this.lblTitulo.AutoSize = true;
            this.lblTitulo.Font = new System.Drawing.Font("Microsoft YaHei UI", 16.2F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitulo.ForeColor = System.Drawing.Color.DarkCyan;
            this.lblTitulo.Location = new System.Drawing.Point(224, 9);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(323, 36);
            this.lblTitulo.TabIndex = 20;
            this.lblTitulo.Text = "Elección de Candidatos";
            // 
            // frmEjercicio2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 479);
            this.Controls.Add(this.lblTitulo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnConteo);
            this.Controls.Add(this.grElecciones);
            this.Controls.Add(this.lblCantidadVotos);
            this.Controls.Add(this.listVotos);
            this.Controls.Add(this.txtIngresoVotos);
            this.Name = "frmEjercicio2";
            this.Text = "Ejercicio 2";
            this.grElecciones.ResumeLayout(false);
            this.grElecciones.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtIngresoVotos;
        private System.Windows.Forms.ListBox listVotos;
        private System.Windows.Forms.Label lblCantidadVotos;
        private System.Windows.Forms.GroupBox grElecciones;
        private System.Windows.Forms.TextBox txtPorcentajeCandidato4;
        private System.Windows.Forms.TextBox txtPorcentajeCandidato3;
        private System.Windows.Forms.TextBox txtPorcentajeCandidato2;
        private System.Windows.Forms.TextBox txtPorcentajeCandidato1;
        private System.Windows.Forms.TextBox txtVotosCandidato4;
        private System.Windows.Forms.TextBox txtVotosCandidato3;
        private System.Windows.Forms.TextBox txtVotosCandidato2;
        private System.Windows.Forms.TextBox txtVotosCandidato1;
        private System.Windows.Forms.Label lblPorcentajeCandidato4;
        private System.Windows.Forms.Label lblPorcentajeCandidato3;
        private System.Windows.Forms.Label lblPorcentajeCandidato2;
        private System.Windows.Forms.Label lblPorcentajeCandidato1;
        private System.Windows.Forms.Label lblVotosCandidato4;
        private System.Windows.Forms.Label lblVotosCandidato3;
        private System.Windows.Forms.Label lblVotosCandidato2;
        private System.Windows.Forms.Label lblVotosCandidato1;
        private System.Windows.Forms.Label lblCandidato4;
        private System.Windows.Forms.Label lblCandidato3;
        private System.Windows.Forms.Label lblCandidato2;
        private System.Windows.Forms.Label lblCandidato1;
        private System.Windows.Forms.Button btnConteo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblTitulo;
    }
}