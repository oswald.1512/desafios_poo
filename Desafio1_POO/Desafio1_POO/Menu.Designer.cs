﻿
namespace Desafio1_POO
{
    partial class frmMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblEjercicio4 = new System.Windows.Forms.Label();
            this.lblEjercicio2 = new System.Windows.Forms.Label();
            this.lblEjercicio3 = new System.Windows.Forms.Label();
            this.lblEjercicio1 = new System.Windows.Forms.Label();
            this.lblTitulo = new System.Windows.Forms.Label();
            this.Ejercicio4 = new System.Windows.Forms.Button();
            this.Ejercicio3 = new System.Windows.Forms.Button();
            this.Ejercicio2 = new System.Windows.Forms.Button();
            this.Ejercicio1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblEjercicio4
            // 
            this.lblEjercicio4.AutoSize = true;
            this.lblEjercicio4.Font = new System.Drawing.Font("Microsoft YaHei UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEjercicio4.Location = new System.Drawing.Point(253, 228);
            this.lblEjercicio4.Name = "lblEjercicio4";
            this.lblEjercicio4.Size = new System.Drawing.Size(94, 23);
            this.lblEjercicio4.TabIndex = 19;
            this.lblEjercicio4.Text = "Ejercicio 4";
            // 
            // lblEjercicio2
            // 
            this.lblEjercicio2.AutoSize = true;
            this.lblEjercicio2.Font = new System.Drawing.Font("Microsoft YaHei UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEjercicio2.Location = new System.Drawing.Point(253, 62);
            this.lblEjercicio2.Name = "lblEjercicio2";
            this.lblEjercicio2.Size = new System.Drawing.Size(94, 23);
            this.lblEjercicio2.TabIndex = 18;
            this.lblEjercicio2.Text = "Ejercicio 2";
            // 
            // lblEjercicio3
            // 
            this.lblEjercicio3.AutoSize = true;
            this.lblEjercicio3.Font = new System.Drawing.Font("Microsoft YaHei UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEjercicio3.Location = new System.Drawing.Point(45, 228);
            this.lblEjercicio3.Name = "lblEjercicio3";
            this.lblEjercicio3.Size = new System.Drawing.Size(94, 23);
            this.lblEjercicio3.TabIndex = 17;
            this.lblEjercicio3.Text = "Ejercicio 3";
            // 
            // lblEjercicio1
            // 
            this.lblEjercicio1.AutoSize = true;
            this.lblEjercicio1.Font = new System.Drawing.Font("Microsoft YaHei UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEjercicio1.Location = new System.Drawing.Point(45, 62);
            this.lblEjercicio1.Name = "lblEjercicio1";
            this.lblEjercicio1.Size = new System.Drawing.Size(94, 23);
            this.lblEjercicio1.TabIndex = 16;
            this.lblEjercicio1.Text = "Ejercicio 1";
            // 
            // lblTitulo
            // 
            this.lblTitulo.AutoSize = true;
            this.lblTitulo.Font = new System.Drawing.Font("Microsoft YaHei UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitulo.Location = new System.Drawing.Point(159, 20);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(81, 31);
            this.lblTitulo.TabIndex = 15;
            this.lblTitulo.Text = "Menú";
            // 
            // Ejercicio4
            // 
            this.Ejercicio4.BackgroundImage = global::Desafio1_POO.Properties.Resources._4;
            this.Ejercicio4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Ejercicio4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Ejercicio4.ForeColor = System.Drawing.Color.Transparent;
            this.Ejercicio4.Location = new System.Drawing.Point(234, 254);
            this.Ejercicio4.Name = "Ejercicio4";
            this.Ejercicio4.Size = new System.Drawing.Size(133, 106);
            this.Ejercicio4.TabIndex = 14;
            this.Ejercicio4.UseVisualStyleBackColor = true;
            this.Ejercicio4.Click += new System.EventHandler(this.Ejercicio4_Click);
            // 
            // Ejercicio3
            // 
            this.Ejercicio3.BackgroundImage = global::Desafio1_POO.Properties.Resources._3;
            this.Ejercicio3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Ejercicio3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Ejercicio3.ForeColor = System.Drawing.Color.Transparent;
            this.Ejercicio3.Location = new System.Drawing.Point(25, 254);
            this.Ejercicio3.Name = "Ejercicio3";
            this.Ejercicio3.Size = new System.Drawing.Size(133, 106);
            this.Ejercicio3.TabIndex = 13;
            this.Ejercicio3.UseVisualStyleBackColor = true;
            this.Ejercicio3.Click += new System.EventHandler(this.Ejercicio3_Click);
            // 
            // Ejercicio2
            // 
            this.Ejercicio2.BackgroundImage = global::Desafio1_POO.Properties.Resources._2;
            this.Ejercicio2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Ejercicio2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Ejercicio2.ForeColor = System.Drawing.Color.Transparent;
            this.Ejercicio2.Location = new System.Drawing.Point(234, 88);
            this.Ejercicio2.Name = "Ejercicio2";
            this.Ejercicio2.Size = new System.Drawing.Size(133, 106);
            this.Ejercicio2.TabIndex = 12;
            this.Ejercicio2.UseVisualStyleBackColor = true;
            this.Ejercicio2.Click += new System.EventHandler(this.Ejercicio2_Click);
            // 
            // Ejercicio1
            // 
            this.Ejercicio1.BackgroundImage = global::Desafio1_POO.Properties.Resources._1;
            this.Ejercicio1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Ejercicio1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Ejercicio1.ForeColor = System.Drawing.Color.Transparent;
            this.Ejercicio1.Location = new System.Drawing.Point(25, 88);
            this.Ejercicio1.Name = "Ejercicio1";
            this.Ejercicio1.Size = new System.Drawing.Size(133, 106);
            this.Ejercicio1.TabIndex = 11;
            this.Ejercicio1.UseVisualStyleBackColor = true;
            this.Ejercicio1.Click += new System.EventHandler(this.Ejercicio1_Click);
            // 
            // frmMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(392, 387);
            this.Controls.Add(this.lblEjercicio4);
            this.Controls.Add(this.lblEjercicio2);
            this.Controls.Add(this.lblEjercicio3);
            this.Controls.Add(this.lblEjercicio1);
            this.Controls.Add(this.lblTitulo);
            this.Controls.Add(this.Ejercicio4);
            this.Controls.Add(this.Ejercicio3);
            this.Controls.Add(this.Ejercicio2);
            this.Controls.Add(this.Ejercicio1);
            this.Name = "frmMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Menu";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblEjercicio4;
        private System.Windows.Forms.Label lblEjercicio2;
        private System.Windows.Forms.Label lblEjercicio3;
        private System.Windows.Forms.Label lblEjercicio1;
        private System.Windows.Forms.Label lblTitulo;
        private System.Windows.Forms.Button Ejercicio4;
        private System.Windows.Forms.Button Ejercicio3;
        private System.Windows.Forms.Button Ejercicio2;
        private System.Windows.Forms.Button Ejercicio1;
    }
}