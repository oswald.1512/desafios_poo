﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Desafio1_POO
{
    public partial class frmResultados : Form
    {
        public frmResultados()
        {
            InitializeComponent();
        }
        internal static string[,] resultados = new string[3, 5];
        private void frmResultados_Load(object sender, EventArgs e)
        {
            double salarioBase;
            bool bono = true;

            //Pasando los datos a una matriz de resultados
            resultados[0, 0] = frmEjercicio4.empleados[0, 0] + " " + frmEjercicio4.empleados[0, 1];
            salarioBase = CalcularSalarioBase(Convert.ToInt32(frmEjercicio4.empleados[0, 2]));
            resultados[0, 1] = CalcularAfp(salarioBase).ToString();
            resultados[0, 2] = CalcularIsss(salarioBase).ToString();
            resultados[0, 3] = CalcularRenta(salarioBase).ToString();
            resultados[0, 4] = (salarioBase - (CalcularAfp(salarioBase) + CalcularIsss(salarioBase) + CalcularRenta(salarioBase))).ToString();

            resultados[1, 0] = frmEjercicio4.empleados[1, 0] + " " + frmEjercicio4.empleados[1, 1];
            salarioBase = CalcularSalarioBase(Convert.ToInt32(frmEjercicio4.empleados[1, 2]));
            resultados[1, 1] = CalcularAfp(salarioBase).ToString();
            resultados[1, 2] = CalcularIsss(salarioBase).ToString();
            resultados[1, 3] = CalcularRenta(salarioBase).ToString();
            resultados[1, 4] = (salarioBase - (CalcularAfp(salarioBase) + CalcularIsss(salarioBase) + CalcularRenta(salarioBase))).ToString();

            resultados[2, 0] = frmEjercicio4.empleados[2, 0] + " " + frmEjercicio4.empleados[2, 1];
            salarioBase = CalcularSalarioBase(Convert.ToInt32(frmEjercicio4.empleados[2, 2]));
            resultados[2, 1] = CalcularAfp(salarioBase).ToString();
            resultados[2, 2] = CalcularIsss(salarioBase).ToString();
            resultados[2, 3] = CalcularRenta(salarioBase).ToString();
            resultados[2, 4] = (salarioBase - (CalcularAfp(salarioBase) + CalcularIsss(salarioBase) + CalcularRenta(salarioBase))).ToString();

            //Condicional para aplicar o no el BONO
            if (frmEjercicio4.empleados[0, 3] == "Gerente" && frmEjercicio4.empleados[1, 3] == "Asistente" && frmEjercicio4.empleados[2, 3] == "Secretaria")
            {
                bono = false;
            }
            else
            {
                resultados[0, 4] = (CalcularSalarioBono(Convert.ToDouble(resultados[0, 4]), frmEjercicio4.empleados[0, 3])).ToString();
                resultados[1, 4] = (CalcularSalarioBono(Convert.ToDouble(resultados[1, 4]), frmEjercicio4.empleados[1, 3])).ToString();
                resultados[2, 4] = (CalcularSalarioBono(Convert.ToDouble(resultados[2, 4]), frmEjercicio4.empleados[2, 3])).ToString();
            }

            //Encontrando quien tiene el salario menor, mayor y cuantos ganan mas de 300
            int iMayor = 0, iMenor = 0, iMayorTrescientos = 0;
            double salarioTemp = 0;
            for (int i = 0; i < 3; i++)
            {
                if (Convert.ToDouble(resultados[i, 4]) > salarioTemp)
                {
                    salarioTemp = Convert.ToDouble(resultados[i, 4]);
                    iMayor = i;
                }
            }

            for (int j = 0; j < 3; j++)
            {
                if (Convert.ToDouble(resultados[j, 4]) < salarioTemp)
                {
                    salarioTemp = Convert.ToDouble(resultados[j, 4]);
                    iMenor = j;
                }
            }

            for (int k = 0; k < 3; k++)
            {
                if (Convert.ToDouble(resultados[k, 4]) > 300)
                {
                    iMayorTrescientos++;
                }
            }

            //Presentando los resultados
            txtNombreE1.Text = resultados[0, 0];
            txtAfpE1.Text = resultados[0, 1];
            txtIssE1.Text = resultados[0, 2];
            txtRentaE1.Text = resultados[0, 3];
            txtSueldoE1.Text = resultados[0, 4];

            txtNombreE2.Text = resultados[1, 0];
            txtAfpE2.Text = resultados[1, 1];
            txtIssE2.Text = resultados[1, 2];
            txtRentaE2.Text = resultados[1, 3];
            txtSueldoE2.Text = resultados[1, 4];

            txtNombreE3.Text = resultados[2, 0];
            txtAfpE3.Text = resultados[2, 1];
            txtIssE3.Text = resultados[2, 2];
            txtRentaE3.Text = resultados[2, 3];
            txtSueldoE3.Text = resultados[2, 4];

            lblEmpleadoMayorSalario.Text = "El empleado con mayor salario es " + resultados[iMayor, 0];
            lblEmpleadoMenorSalario.Text = "El empleado con menor salario es " + resultados[iMenor, 0];
            lblSalarioMayor.Text = iMayorTrescientos + " empleados ganan más de $300.";

            if (bono)
            {
                lblBono.Text = "SE HA APLICADO BONOS";
            }
            else
            {
                lblBono.Text = "NO SE HA APLICADO BONOS";
            }
        }

        //funcion para calcular el salario base
        private double CalcularSalarioBase(int horas)
        {
            double salario;
            if (horas <= 160)
            {
                salario = horas * 9.75;
            }
            else
            {
                salario = 160 * 9.75;
                horas = horas - 160;
                salario = salario + (horas * 11.50);
            }
            return salario;
        }
        //funcion para calcular el afp
        private double CalcularAfp(double salario)
        {
            double afp;
            afp = salario * 0.0688;
            return afp;
        }
        //funcion para calcular el isss
        private double CalcularIsss(double salario)
        {
            double isss;
            isss = salario * 0.0525;
            return isss;
        }
        //funcion para calcular la renta
        private double CalcularRenta(double salario)
        {
            double renta;
            renta = salario * 0.1;
            return renta;
        }

        //funcion para calcular el salario bono
        private double CalcularSalarioBono(double salario, string cargo)
        {
            double salarioFinal = 0;
            if (cargo == "Gerente")
            {
                salarioFinal = salario + (salario * 0.1);
            }
            else if (cargo == "Asistente")
            {
                salarioFinal = salario + (salario * 0.05);
            }
            else if (cargo == "Secretaria")
            {
                salarioFinal = salario + (salario * 0.03);
            }
            return salarioFinal;
        }
    }
}
