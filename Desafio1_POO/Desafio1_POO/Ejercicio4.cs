﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Desafio1_POO
{
    public partial class frmEjercicio4 : Form
    {
        public frmEjercicio4()
        {
            InitializeComponent();
        }
        internal static string[,] empleados = new string[3, 4];
        int i = 0;
        private void frmEjercicio4_Load(object sender, EventArgs e)
        {
            cboCargo.Items.Add("Gerente");
            cboCargo.Items.Add("Asistente");
            cboCargo.Items.Add("Secretaria");
        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            String nombres, apellidos, cargo, horas;
            if (txtNombres.Text == "" || txtNombres.Text == null || txtNombres.Text == " " || txtApellidos.Text == "" || txtApellidos.Text == null || txtApellidos.Text == " " || txtHoras.Text == "" || txtHoras.Text == null || txtHoras.Text == " " || cboCargo.SelectedIndex.Equals(-1))
            {
                MessageBox.Show("Llene correctamente los campos", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                nombres = txtNombres.Text;
                apellidos = txtApellidos.Text;
                cargo = cboCargo.SelectedItem.ToString();
                horas = txtHoras.Text;
                //Llenando la matriz de empleados 
                if (i < 3)
                {
                    empleados[i, 0] = nombres;
                    empleados[i, 1] = apellidos;
                    empleados[i, 2] = horas;
                    empleados[i, 3] = cargo;

                    MessageBox.Show("Empleado agregado correctamente.", "Registro de empleados");

                    txtNombres.Text = "";
                    txtApellidos.Text = "";
                    txtHoras.Text = "";
                    cboCargo.SelectedIndex = -1;
                    i++;

                    //Redirigiendo al formulario de resultados al haber llebano los datos de 3 empleados
                    if (i == 3)
                    {
                        frmResultados frm = new frmResultados();
                        frm.Show();
                        this.Hide();
                    }
                }
            }
        }

        private void txtHoras_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {
                MessageBox.Show("Solo se permiten números.", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void txtNombres_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {
                MessageBox.Show("Solo se permiten letras.", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void txtApellidos_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {
                MessageBox.Show("Solo se permiten letras.", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }
    }
}
