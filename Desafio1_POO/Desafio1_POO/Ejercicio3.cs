﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Desafio1_POO
{
    public partial class frmEjercicio3 : Form
    {
        public frmEjercicio3()
        {
            InitializeComponent();
        }
        //Declaración de variables
        int bandera = 0;
        int[] numeros = new int[4];
        int mayor = 0;
        int menor = 0;
        int sumaMayor = 0;
        int restaMayor = 0;
        string verificadorNumero = "0";
        private void txtNumeros_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Validando solo números y sus decimales
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                MessageBox.Show("Solo se puede colocar números enteros y positivos", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
            //Validar solo 4 números
            if (bandera<4)
            {
                //Ingresar datos con la tecla ENTER
                if ((int)e.KeyChar == (int)Keys.Enter)
                {
                    //Validando que no este vacío
                    if (!string.IsNullOrEmpty(txtNumeros.Text))
                    {
                        //Validando no repetir el valor
                        for (int i = 0; i < listNumeros.Items.Count; i++)
                        {
                            verificadorNumero = listNumeros.Items[i].ToString();
                            if (verificadorNumero == txtNumeros.Text)
                            {
                                MessageBox.Show("No se pueden repetir los números", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                txtNumeros.Clear();
                                txtNumeros.Focus();
                            }
                        }
                        //Validando que no sea Cero
                        if (txtNumeros.Text == "0")
                        {
                            MessageBox.Show("No se permite el valor de cero, ingresar nuevamente", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            txtNumeros.Clear();
                            txtNumeros.Focus();
                            e.Handled = true;
                            return;
                        }
                        //Validando el ingreso correcto de datos 
                        if (txtNumeros.Text != "0" && !string.IsNullOrEmpty(txtNumeros.Text))
                        {
                            bandera++;
                            listNumeros.Items.Add(txtNumeros.Text);
                            txtNumeros.Clear();
                            txtNumeros.Focus();

                            for (int i = 0; i < listNumeros.Items.Count; i++)
                            {
                                numeros[i] = int.Parse(listNumeros.Items[i].ToString());
                            }
                        }    
                    }
                    else
                    {
                        MessageBox.Show("Debe ingresar un número", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
            }
            else
            {
                MessageBox.Show("No pueden ingresar más de 4 números", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
            if (bandera==4)
            {
                    if (numeros.Sum() >= 200)
                    {
                    MessageBox.Show("La sumatoria de los números es mayor o igual a 200, Reingresar los números", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    bandera = 0;
                    listNumeros.Items.Clear();
                }
                else
                {
                    btnCalculos.Enabled = true;
                }        
            }
        }

        private void btnCalculos_Click(object sender, EventArgs e)
        {
            //Encontrando número mayor y menor
                Array.Sort(numeros);
                mayor = numeros[3];
                menor = numeros[0];
                txtNumeroMayor.Text = Convert.ToString(mayor);
                txtNumeroMenor.Text = Convert.ToString(menor);

            //Verificando si se suma o no al número mayor
            if (menor > 10)
            {
                sumaMayor = mayor + 10;
                txtSumaMayor.Text = Convert.ToString(sumaMayor);
            }
            else
            {
                txtSumaMayor.Text = "El número menor no es mayor a 10";
            }
            //Verificando si se resta o no al número menor
            if (mayor < 50)
            {
                restaMayor = menor - 5;
                txtRestaMenor.Text = Convert.ToString(restaMayor);
            }
            else
            {
                txtRestaMenor.Text = "El número mayor no es menor a 50";
            }
        }
    }
}
