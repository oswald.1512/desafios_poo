﻿
namespace Desafio1_POO
{
    partial class frmEjercicio3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtNumeros = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.listNumeros = new System.Windows.Forms.ListBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnCalculos = new System.Windows.Forms.Button();
            this.lblResta = new System.Windows.Forms.Label();
            this.lblSuma = new System.Windows.Forms.Label();
            this.lblMenor = new System.Windows.Forms.Label();
            this.lblMayor = new System.Windows.Forms.Label();
            this.txtNumeroMayor = new System.Windows.Forms.TextBox();
            this.txtRestaMenor = new System.Windows.Forms.TextBox();
            this.txtNumeroMenor = new System.Windows.Forms.TextBox();
            this.txtSumaMayor = new System.Windows.Forms.TextBox();
            this.lblIndicacion = new System.Windows.Forms.Label();
            this.lblNumerosIngresados = new System.Windows.Forms.Label();
            this.lblTitulo = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtNumeros
            // 
            this.txtNumeros.Location = new System.Drawing.Point(184, 91);
            this.txtNumeros.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtNumeros.Name = "txtNumeros";
            this.txtNumeros.Size = new System.Drawing.Size(236, 27);
            this.txtNumeros.TabIndex = 15;
            this.txtNumeros.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNumeros_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 94);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(136, 20);
            this.label1.TabIndex = 14;
            this.label1.Text = "Ingresar número:";
            // 
            // listNumeros
            // 
            this.listNumeros.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listNumeros.BackColor = System.Drawing.Color.Azure;
            this.listNumeros.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listNumeros.Cursor = System.Windows.Forms.Cursors.Hand;
            this.listNumeros.FormattingEnabled = true;
            this.listNumeros.ItemHeight = 20;
            this.listNumeros.Location = new System.Drawing.Point(13, 201);
            this.listNumeros.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.listNumeros.Name = "listNumeros";
            this.listNumeros.Size = new System.Drawing.Size(253, 140);
            this.listNumeros.TabIndex = 13;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnCalculos);
            this.groupBox1.Controls.Add(this.lblResta);
            this.groupBox1.Controls.Add(this.lblSuma);
            this.groupBox1.Controls.Add(this.lblMenor);
            this.groupBox1.Controls.Add(this.lblMayor);
            this.groupBox1.Controls.Add(this.txtNumeroMayor);
            this.groupBox1.Controls.Add(this.txtRestaMenor);
            this.groupBox1.Controls.Add(this.txtNumeroMenor);
            this.groupBox1.Controls.Add(this.txtSumaMayor);
            this.groupBox1.Location = new System.Drawing.Point(293, 143);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Size = new System.Drawing.Size(754, 346);
            this.groupBox1.TabIndex = 16;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Cálculos según condición ";
            // 
            // btnCalculos
            // 
            this.btnCalculos.BackColor = System.Drawing.Color.Transparent;
            this.btnCalculos.BackgroundImage = global::Desafio1_POO.Properties.Resources.lupa;
            this.btnCalculos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnCalculos.Enabled = false;
            this.btnCalculos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCalculos.ForeColor = System.Drawing.Color.Transparent;
            this.btnCalculos.Location = new System.Drawing.Point(616, 58);
            this.btnCalculos.Margin = new System.Windows.Forms.Padding(4);
            this.btnCalculos.Name = "btnCalculos";
            this.btnCalculos.Size = new System.Drawing.Size(122, 97);
            this.btnCalculos.TabIndex = 9;
            this.btnCalculos.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnCalculos.UseVisualStyleBackColor = false;
            this.btnCalculos.Click += new System.EventHandler(this.btnCalculos_Click);
            // 
            // lblResta
            // 
            this.lblResta.AutoSize = true;
            this.lblResta.Location = new System.Drawing.Point(8, 282);
            this.lblResta.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblResta.Name = "lblResta";
            this.lblResta.Size = new System.Drawing.Size(180, 20);
            this.lblResta.TabIndex = 10;
            this.lblResta.Text = "Resta a número menor";
            // 
            // lblSuma
            // 
            this.lblSuma.AutoSize = true;
            this.lblSuma.Location = new System.Drawing.Point(12, 208);
            this.lblSuma.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSuma.Name = "lblSuma";
            this.lblSuma.Size = new System.Drawing.Size(178, 20);
            this.lblSuma.TabIndex = 9;
            this.lblSuma.Text = "Suma a número mayor";
            // 
            // lblMenor
            // 
            this.lblMenor.AutoSize = true;
            this.lblMenor.Location = new System.Drawing.Point(9, 132);
            this.lblMenor.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMenor.Name = "lblMenor";
            this.lblMenor.Size = new System.Drawing.Size(120, 20);
            this.lblMenor.TabIndex = 8;
            this.lblMenor.Text = "Número menor";
            // 
            // lblMayor
            // 
            this.lblMayor.AutoSize = true;
            this.lblMayor.Location = new System.Drawing.Point(8, 58);
            this.lblMayor.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMayor.Name = "lblMayor";
            this.lblMayor.Size = new System.Drawing.Size(119, 20);
            this.lblMayor.TabIndex = 7;
            this.lblMayor.Text = "Número mayor";
            // 
            // txtNumeroMayor
            // 
            this.txtNumeroMayor.Location = new System.Drawing.Point(266, 58);
            this.txtNumeroMayor.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtNumeroMayor.Name = "txtNumeroMayor";
            this.txtNumeroMayor.ReadOnly = true;
            this.txtNumeroMayor.Size = new System.Drawing.Size(330, 27);
            this.txtNumeroMayor.TabIndex = 3;
            // 
            // txtRestaMenor
            // 
            this.txtRestaMenor.Location = new System.Drawing.Point(266, 278);
            this.txtRestaMenor.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtRestaMenor.Name = "txtRestaMenor";
            this.txtRestaMenor.ReadOnly = true;
            this.txtRestaMenor.Size = new System.Drawing.Size(330, 27);
            this.txtRestaMenor.TabIndex = 6;
            // 
            // txtNumeroMenor
            // 
            this.txtNumeroMenor.Location = new System.Drawing.Point(266, 128);
            this.txtNumeroMenor.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtNumeroMenor.Name = "txtNumeroMenor";
            this.txtNumeroMenor.ReadOnly = true;
            this.txtNumeroMenor.Size = new System.Drawing.Size(330, 27);
            this.txtNumeroMenor.TabIndex = 4;
            // 
            // txtSumaMayor
            // 
            this.txtSumaMayor.Location = new System.Drawing.Point(266, 200);
            this.txtSumaMayor.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtSumaMayor.Name = "txtSumaMayor";
            this.txtSumaMayor.ReadOnly = true;
            this.txtSumaMayor.Size = new System.Drawing.Size(330, 27);
            this.txtSumaMayor.TabIndex = 5;
            // 
            // lblIndicacion
            // 
            this.lblIndicacion.AutoSize = true;
            this.lblIndicacion.Location = new System.Drawing.Point(452, 94);
            this.lblIndicacion.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblIndicacion.Name = "lblIndicacion";
            this.lblIndicacion.Size = new System.Drawing.Size(313, 20);
            this.lblIndicacion.TabIndex = 17;
            this.lblIndicacion.Text = "(Presione ENTER para ingresar número)";
            // 
            // lblNumerosIngresados
            // 
            this.lblNumerosIngresados.AutoSize = true;
            this.lblNumerosIngresados.Location = new System.Drawing.Point(13, 168);
            this.lblNumerosIngresados.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNumerosIngresados.Name = "lblNumerosIngresados";
            this.lblNumerosIngresados.Size = new System.Drawing.Size(169, 20);
            this.lblNumerosIngresados.TabIndex = 18;
            this.lblNumerosIngresados.Text = "Números ingresados:";
            // 
            // lblTitulo
            // 
            this.lblTitulo.AutoSize = true;
            this.lblTitulo.Font = new System.Drawing.Font("Microsoft YaHei UI", 16.2F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitulo.ForeColor = System.Drawing.Color.Chocolate;
            this.lblTitulo.Location = new System.Drawing.Point(298, 9);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(467, 36);
            this.lblTitulo.TabIndex = 19;
            this.lblTitulo.Text = "Cálculos de números con arreglos";
            // 
            // frmEjercicio3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1044, 514);
            this.Controls.Add(this.lblTitulo);
            this.Controls.Add(this.lblNumerosIngresados);
            this.Controls.Add(this.lblIndicacion);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.txtNumeros);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.listNumeros);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmEjercicio3";
            this.Text = "Ejercicio 3";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox txtNumeros;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox listNumeros;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnCalculos;
        private System.Windows.Forms.Label lblResta;
        private System.Windows.Forms.Label lblSuma;
        private System.Windows.Forms.Label lblMenor;
        private System.Windows.Forms.Label lblMayor;
        private System.Windows.Forms.TextBox txtNumeroMayor;
        private System.Windows.Forms.TextBox txtRestaMenor;
        private System.Windows.Forms.TextBox txtNumeroMenor;
        private System.Windows.Forms.TextBox txtSumaMayor;
        private System.Windows.Forms.Label lblIndicacion;
        private System.Windows.Forms.Label lblNumerosIngresados;
        private System.Windows.Forms.Label lblTitulo;
    }
}