﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Desafio1_POO
{
    public partial class frmEjercicio2 : Form
    {
        public frmEjercicio2()
        {
            InitializeComponent();
        }
        int verificadorCandidato1 = 0;
        int verificadorCandidato2 = 0;
        int verificadorCandidato3 = 0;
        int verificadorCandidato4 = 0;
        double porcentajeCandidato1 = 0.0;
        double porcentajeCandidato2 = 0.0;
        double porcentajeCandidato3 = 0.0;
        double porcentajeCandidato4 = 0.0;
        private void txtIngresoVotos_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Validando solo números y la coma
            if (!char.IsControl(e.KeyChar) && (e.KeyChar != '1') && (e.KeyChar != '2') && (e.KeyChar != '3') && (e.KeyChar != '4') && (e.KeyChar != ','))
            {
                MessageBox.Show("Solo se permiten números de 1 al 4 que los separen comas", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void btnConteo_Click(object sender, EventArgs e)
        {
            //Validando que no este vacío
            if (!string.IsNullOrEmpty(txtIngresoVotos.Text))
            {
                
                string cantidadVotos = txtIngresoVotos.Text;
                char separador = ',';
                String[] votos = cantidadVotos.Split(separador);
                foreach (string votado in votos)
                {
                    listVotos.Items.Add(votado);
                }
                txtIngresoVotos.Text = "";
                for (int i=0; i< votos.Length; i++)
                {
                    
                    if (votos[i]== "1")
                    {
                        verificadorCandidato1++;
                    }
                    if (votos[i] == "2")
                    {
                        verificadorCandidato2++;
                    }
                    if (votos[i] == "3")
                    {
                        verificadorCandidato3++;
                    }
                    if (votos[i] == "4")
                    {
                        verificadorCandidato4++;
                    }
                }
                txtVotosCandidato1.Text = Convert.ToString(verificadorCandidato1);
                txtVotosCandidato2.Text = Convert.ToString(verificadorCandidato2);
                txtVotosCandidato3.Text = Convert.ToString(verificadorCandidato3);
                txtVotosCandidato4.Text = Convert.ToString(verificadorCandidato4);

                porcentajeCandidato1 = Math.Round(verificadorCandidato1 * (100.00 / votos.Length), 2);
                porcentajeCandidato2 = Math.Round(verificadorCandidato2 * (100.00 / votos.Length), 2);
                porcentajeCandidato3 = Math.Round(verificadorCandidato3 * (100.00 / votos.Length), 2);
                porcentajeCandidato4 = Math.Round(verificadorCandidato4 * (100.00 / votos.Length), 2);

                txtPorcentajeCandidato1.Text = Convert.ToString(porcentajeCandidato1);
                txtPorcentajeCandidato2.Text = Convert.ToString(porcentajeCandidato2);
                txtPorcentajeCandidato3.Text = Convert.ToString(porcentajeCandidato3);
                txtPorcentajeCandidato4.Text = Convert.ToString(porcentajeCandidato4);
            }
            else
            {
                MessageBox.Show("Debe ingresar los votos", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

            }
        }
    }
}
