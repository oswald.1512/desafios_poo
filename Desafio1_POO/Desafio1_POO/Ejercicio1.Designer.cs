﻿
namespace Desafio1_POO
{
    partial class frmEjercicio1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblValorX2 = new System.Windows.Forms.Label();
            this.lblValorX1 = new System.Windows.Forms.Label();
            this.lblRespuesta2 = new System.Windows.Forms.Label();
            this.lblValorC = new System.Windows.Forms.Label();
            this.lblValorB = new System.Windows.Forms.Label();
            this.txtC = new System.Windows.Forms.TextBox();
            this.txtB = new System.Windows.Forms.TextBox();
            this.lblTitulo = new System.Windows.Forms.Label();
            this.txtA = new System.Windows.Forms.TextBox();
            this.lblRespuesta1 = new System.Windows.Forms.Label();
            this.lblValorA = new System.Windows.Forms.Label();
            this.btnCalcular = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblValorX2
            // 
            this.lblValorX2.AutoSize = true;
            this.lblValorX2.Location = new System.Drawing.Point(380, 199);
            this.lblValorX2.Name = "lblValorX2";
            this.lblValorX2.Size = new System.Drawing.Size(37, 17);
            this.lblValorX2.TabIndex = 75;
            this.lblValorX2.Text = "X2 =";
            this.lblValorX2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblValorX1
            // 
            this.lblValorX1.AutoSize = true;
            this.lblValorX1.Location = new System.Drawing.Point(32, 199);
            this.lblValorX1.Name = "lblValorX1";
            this.lblValorX1.Size = new System.Drawing.Size(37, 17);
            this.lblValorX1.TabIndex = 74;
            this.lblValorX1.Text = "X1 =";
            this.lblValorX1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblRespuesta2
            // 
            this.lblRespuesta2.AutoSize = true;
            this.lblRespuesta2.Font = new System.Drawing.Font("Microsoft YaHei UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRespuesta2.Location = new System.Drawing.Point(423, 194);
            this.lblRespuesta2.Name = "lblRespuesta2";
            this.lblRespuesta2.Size = new System.Drawing.Size(0, 23);
            this.lblRespuesta2.TabIndex = 73;
            // 
            // lblValorC
            // 
            this.lblValorC.AutoSize = true;
            this.lblValorC.Location = new System.Drawing.Point(519, 98);
            this.lblValorC.Name = "lblValorC";
            this.lblValorC.Size = new System.Drawing.Size(76, 17);
            this.lblValorC.TabIndex = 72;
            this.lblValorC.Text = "Valor de c:";
            this.lblValorC.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblValorB
            // 
            this.lblValorB.AutoSize = true;
            this.lblValorB.Location = new System.Drawing.Point(262, 96);
            this.lblValorB.Name = "lblValorB";
            this.lblValorB.Size = new System.Drawing.Size(77, 17);
            this.lblValorB.TabIndex = 71;
            this.lblValorB.Text = "Valor de b:";
            this.lblValorB.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtC
            // 
            this.txtC.Location = new System.Drawing.Point(608, 93);
            this.txtC.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtC.Name = "txtC";
            this.txtC.Size = new System.Drawing.Size(102, 22);
            this.txtC.TabIndex = 70;
            this.txtC.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtC_KeyPress);
            // 
            // txtB
            // 
            this.txtB.Location = new System.Drawing.Point(354, 93);
            this.txtB.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtB.Name = "txtB";
            this.txtB.Size = new System.Drawing.Size(102, 22);
            this.txtB.TabIndex = 69;
            this.txtB.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtB_KeyPress);
            // 
            // lblTitulo
            // 
            this.lblTitulo.AutoSize = true;
            this.lblTitulo.Font = new System.Drawing.Font("Microsoft YaHei UI", 18F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitulo.ForeColor = System.Drawing.Color.Green;
            this.lblTitulo.Location = new System.Drawing.Point(223, 25);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(291, 39);
            this.lblTitulo.TabIndex = 67;
            this.lblTitulo.Text = "Fórmula cuadrática";
            this.lblTitulo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtA
            // 
            this.txtA.Location = new System.Drawing.Point(114, 93);
            this.txtA.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtA.Name = "txtA";
            this.txtA.Size = new System.Drawing.Size(102, 22);
            this.txtA.TabIndex = 66;
            this.txtA.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtA_KeyPress);
            // 
            // lblRespuesta1
            // 
            this.lblRespuesta1.AutoSize = true;
            this.lblRespuesta1.Font = new System.Drawing.Font("Microsoft YaHei UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRespuesta1.Location = new System.Drawing.Point(75, 193);
            this.lblRespuesta1.Name = "lblRespuesta1";
            this.lblRespuesta1.Size = new System.Drawing.Size(0, 23);
            this.lblRespuesta1.TabIndex = 65;
            // 
            // lblValorA
            // 
            this.lblValorA.AutoSize = true;
            this.lblValorA.Location = new System.Drawing.Point(18, 93);
            this.lblValorA.Name = "lblValorA";
            this.lblValorA.Size = new System.Drawing.Size(77, 17);
            this.lblValorA.TabIndex = 64;
            this.lblValorA.Text = "Valor de a:";
            this.lblValorA.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnCalcular
            // 
            this.btnCalcular.BackColor = System.Drawing.Color.Transparent;
            this.btnCalcular.BackgroundImage = global::Desafio1_POO.Properties.Resources.calculador;
            this.btnCalcular.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnCalcular.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCalcular.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCalcular.ForeColor = System.Drawing.Color.Transparent;
            this.btnCalcular.Location = new System.Drawing.Point(270, 269);
            this.btnCalcular.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnCalcular.Name = "btnCalcular";
            this.btnCalcular.Size = new System.Drawing.Size(189, 128);
            this.btnCalcular.TabIndex = 68;
            this.btnCalcular.UseVisualStyleBackColor = false;
            this.btnCalcular.Click += new System.EventHandler(this.btnCalcular_Click);
            // 
            // frmEjercicio1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(736, 451);
            this.Controls.Add(this.lblValorX2);
            this.Controls.Add(this.lblValorX1);
            this.Controls.Add(this.lblRespuesta2);
            this.Controls.Add(this.lblValorC);
            this.Controls.Add(this.lblValorB);
            this.Controls.Add(this.txtC);
            this.Controls.Add(this.txtB);
            this.Controls.Add(this.btnCalcular);
            this.Controls.Add(this.lblTitulo);
            this.Controls.Add(this.txtA);
            this.Controls.Add(this.lblRespuesta1);
            this.Controls.Add(this.lblValorA);
            this.Name = "frmEjercicio1";
            this.Text = "Ejercicio 1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblValorX2;
        private System.Windows.Forms.Label lblValorX1;
        private System.Windows.Forms.Label lblRespuesta2;
        private System.Windows.Forms.Label lblValorC;
        private System.Windows.Forms.Label lblValorB;
        private System.Windows.Forms.TextBox txtC;
        private System.Windows.Forms.TextBox txtB;
        private System.Windows.Forms.Button btnCalcular;
        private System.Windows.Forms.Label lblTitulo;
        private System.Windows.Forms.TextBox txtA;
        private System.Windows.Forms.Label lblRespuesta1;
        private System.Windows.Forms.Label lblValorA;
    }
}

