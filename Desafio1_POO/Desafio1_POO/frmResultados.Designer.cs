﻿
namespace Desafio1_POO
{
    partial class frmResultados
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblBono = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.lblSalarioMayor = new System.Windows.Forms.Label();
            this.lblEmpleadoMenorSalario = new System.Windows.Forms.Label();
            this.lblEmpleadoMayorSalario = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtIssE3 = new System.Windows.Forms.TextBox();
            this.txtSueldoE3 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtNombreE3 = new System.Windows.Forms.TextBox();
            this.txtRentaE3 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtAfpE3 = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtIssE2 = new System.Windows.Forms.TextBox();
            this.txtSueldoE2 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtNombreE2 = new System.Windows.Forms.TextBox();
            this.txtRentaE2 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtAfpE2 = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtIssE1 = new System.Windows.Forms.TextBox();
            this.txtSueldoE1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtNombreE1 = new System.Windows.Forms.TextBox();
            this.txtRentaE1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtAfpE1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblBono
            // 
            this.lblBono.AutoSize = true;
            this.lblBono.Location = new System.Drawing.Point(400, 42);
            this.lblBono.Name = "lblBono";
            this.lblBono.Size = new System.Drawing.Size(23, 13);
            this.lblBono.TabIndex = 22;
            this.lblBono.Text = "EM";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.lblSalarioMayor);
            this.groupBox4.Controls.Add(this.lblEmpleadoMenorSalario);
            this.groupBox4.Controls.Add(this.lblEmpleadoMayorSalario);
            this.groupBox4.Location = new System.Drawing.Point(207, 295);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(535, 144);
            this.groupBox4.TabIndex = 21;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Más información";
            // 
            // lblSalarioMayor
            // 
            this.lblSalarioMayor.AutoSize = true;
            this.lblSalarioMayor.Location = new System.Drawing.Point(6, 104);
            this.lblSalarioMayor.Name = "lblSalarioMayor";
            this.lblSalarioMayor.Size = new System.Drawing.Size(23, 13);
            this.lblSalarioMayor.TabIndex = 15;
            this.lblSalarioMayor.Text = "EM";
            // 
            // lblEmpleadoMenorSalario
            // 
            this.lblEmpleadoMenorSalario.AutoSize = true;
            this.lblEmpleadoMenorSalario.Location = new System.Drawing.Point(6, 67);
            this.lblEmpleadoMenorSalario.Name = "lblEmpleadoMenorSalario";
            this.lblEmpleadoMenorSalario.Size = new System.Drawing.Size(23, 13);
            this.lblEmpleadoMenorSalario.TabIndex = 14;
            this.lblEmpleadoMenorSalario.Text = "EM";
            // 
            // lblEmpleadoMayorSalario
            // 
            this.lblEmpleadoMayorSalario.AutoSize = true;
            this.lblEmpleadoMayorSalario.Location = new System.Drawing.Point(6, 27);
            this.lblEmpleadoMayorSalario.Name = "lblEmpleadoMayorSalario";
            this.lblEmpleadoMayorSalario.Size = new System.Drawing.Size(23, 13);
            this.lblEmpleadoMayorSalario.TabIndex = 13;
            this.lblEmpleadoMayorSalario.Text = "EM";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtIssE3);
            this.groupBox3.Controls.Add(this.txtSueldoE3);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.txtNombreE3);
            this.groupBox3.Controls.Add(this.txtRentaE3);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.txtAfpE3);
            this.groupBox3.Location = new System.Drawing.Point(629, 58);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(279, 222);
            this.groupBox3.TabIndex = 19;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Empleado 3";
            // 
            // txtIssE3
            // 
            this.txtIssE3.Location = new System.Drawing.Point(109, 61);
            this.txtIssE3.Name = "txtIssE3";
            this.txtIssE3.ReadOnly = true;
            this.txtIssE3.Size = new System.Drawing.Size(100, 20);
            this.txtIssE3.TabIndex = 4;
            // 
            // txtSueldoE3
            // 
            this.txtSueldoE3.Location = new System.Drawing.Point(109, 174);
            this.txtSueldoE3.Name = "txtSueldoE3";
            this.txtSueldoE3.ReadOnly = true;
            this.txtSueldoE3.Size = new System.Drawing.Size(100, 20);
            this.txtSueldoE3.TabIndex = 10;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(50, 29);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(47, 13);
            this.label12.TabIndex = 1;
            this.label12.Text = "Nombre:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(36, 177);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(67, 13);
            this.label13.TabIndex = 9;
            this.label13.Text = "Sueldo neto:";
            // 
            // txtNombreE3
            // 
            this.txtNombreE3.Location = new System.Drawing.Point(109, 26);
            this.txtNombreE3.Name = "txtNombreE3";
            this.txtNombreE3.ReadOnly = true;
            this.txtNombreE3.Size = new System.Drawing.Size(100, 20);
            this.txtNombreE3.TabIndex = 2;
            // 
            // txtRentaE3
            // 
            this.txtRentaE3.Location = new System.Drawing.Point(109, 137);
            this.txtRentaE3.Name = "txtRentaE3";
            this.txtRentaE3.ReadOnly = true;
            this.txtRentaE3.Size = new System.Drawing.Size(100, 20);
            this.txtRentaE3.TabIndex = 8;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(50, 64);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(34, 13);
            this.label14.TabIndex = 3;
            this.label14.Text = "ISSS:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(50, 140);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(39, 13);
            this.label15.TabIndex = 7;
            this.label15.Text = "Renta:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(50, 100);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(30, 13);
            this.label16.TabIndex = 5;
            this.label16.Text = "AFP:";
            // 
            // txtAfpE3
            // 
            this.txtAfpE3.Location = new System.Drawing.Point(109, 97);
            this.txtAfpE3.Name = "txtAfpE3";
            this.txtAfpE3.ReadOnly = true;
            this.txtAfpE3.Size = new System.Drawing.Size(100, 20);
            this.txtAfpE3.TabIndex = 6;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtIssE2);
            this.groupBox2.Controls.Add(this.txtSueldoE2);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.txtNombreE2);
            this.groupBox2.Controls.Add(this.txtRentaE2);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.txtAfpE2);
            this.groupBox2.Location = new System.Drawing.Point(320, 58);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(279, 222);
            this.groupBox2.TabIndex = 20;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Empleado 2";
            // 
            // txtIssE2
            // 
            this.txtIssE2.Location = new System.Drawing.Point(109, 61);
            this.txtIssE2.Name = "txtIssE2";
            this.txtIssE2.ReadOnly = true;
            this.txtIssE2.Size = new System.Drawing.Size(100, 20);
            this.txtIssE2.TabIndex = 4;
            // 
            // txtSueldoE2
            // 
            this.txtSueldoE2.Location = new System.Drawing.Point(109, 174);
            this.txtSueldoE2.Name = "txtSueldoE2";
            this.txtSueldoE2.ReadOnly = true;
            this.txtSueldoE2.Size = new System.Drawing.Size(100, 20);
            this.txtSueldoE2.TabIndex = 10;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(50, 29);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Nombre:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(36, 177);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(67, 13);
            this.label8.TabIndex = 9;
            this.label8.Text = "Sueldo neto:";
            // 
            // txtNombreE2
            // 
            this.txtNombreE2.Location = new System.Drawing.Point(109, 26);
            this.txtNombreE2.Name = "txtNombreE2";
            this.txtNombreE2.ReadOnly = true;
            this.txtNombreE2.Size = new System.Drawing.Size(100, 20);
            this.txtNombreE2.TabIndex = 2;
            // 
            // txtRentaE2
            // 
            this.txtRentaE2.Location = new System.Drawing.Point(109, 137);
            this.txtRentaE2.Name = "txtRentaE2";
            this.txtRentaE2.ReadOnly = true;
            this.txtRentaE2.Size = new System.Drawing.Size(100, 20);
            this.txtRentaE2.TabIndex = 8;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(50, 64);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(34, 13);
            this.label9.TabIndex = 3;
            this.label9.Text = "ISSS:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(50, 140);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(39, 13);
            this.label10.TabIndex = 7;
            this.label10.Text = "Renta:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(50, 100);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(30, 13);
            this.label11.TabIndex = 5;
            this.label11.Text = "AFP:";
            // 
            // txtAfpE2
            // 
            this.txtAfpE2.Location = new System.Drawing.Point(109, 97);
            this.txtAfpE2.Name = "txtAfpE2";
            this.txtAfpE2.ReadOnly = true;
            this.txtAfpE2.Size = new System.Drawing.Size(100, 20);
            this.txtAfpE2.TabIndex = 6;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtIssE1);
            this.groupBox1.Controls.Add(this.txtSueldoE1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtNombreE1);
            this.groupBox1.Controls.Add(this.txtRentaE1);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtAfpE1);
            this.groupBox1.Location = new System.Drawing.Point(13, 58);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(279, 222);
            this.groupBox1.TabIndex = 18;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Empleado 1";
            // 
            // txtIssE1
            // 
            this.txtIssE1.Location = new System.Drawing.Point(109, 61);
            this.txtIssE1.Name = "txtIssE1";
            this.txtIssE1.ReadOnly = true;
            this.txtIssE1.Size = new System.Drawing.Size(100, 20);
            this.txtIssE1.TabIndex = 4;
            // 
            // txtSueldoE1
            // 
            this.txtSueldoE1.Location = new System.Drawing.Point(109, 174);
            this.txtSueldoE1.Name = "txtSueldoE1";
            this.txtSueldoE1.ReadOnly = true;
            this.txtSueldoE1.Size = new System.Drawing.Size(100, 20);
            this.txtSueldoE1.TabIndex = 10;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(50, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nombre:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(36, 177);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Sueldo neto:";
            // 
            // txtNombreE1
            // 
            this.txtNombreE1.Location = new System.Drawing.Point(109, 26);
            this.txtNombreE1.Name = "txtNombreE1";
            this.txtNombreE1.ReadOnly = true;
            this.txtNombreE1.Size = new System.Drawing.Size(100, 20);
            this.txtNombreE1.TabIndex = 2;
            // 
            // txtRentaE1
            // 
            this.txtRentaE1.Location = new System.Drawing.Point(109, 137);
            this.txtRentaE1.Name = "txtRentaE1";
            this.txtRentaE1.ReadOnly = true;
            this.txtRentaE1.Size = new System.Drawing.Size(100, 20);
            this.txtRentaE1.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(50, 64);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "ISSS:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(50, 140);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(39, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Renta:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(50, 100);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "AFP:";
            // 
            // txtAfpE1
            // 
            this.txtAfpE1.Location = new System.Drawing.Point(109, 97);
            this.txtAfpE1.Name = "txtAfpE1";
            this.txtAfpE1.ReadOnly = true;
            this.txtAfpE1.Size = new System.Drawing.Size(100, 20);
            this.txtAfpE1.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft YaHei UI", 18F, System.Drawing.FontStyle.Italic);
            this.label1.Location = new System.Drawing.Point(367, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(139, 31);
            this.label1.TabIndex = 17;
            this.label1.Text = "Resultados";
            // 
            // frmResultados
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(923, 459);
            this.Controls.Add(this.lblBono);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Name = "frmResultados";
            this.Text = "frmResultados";
            this.Load += new System.EventHandler(this.frmResultados_Load);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblBono;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label lblSalarioMayor;
        private System.Windows.Forms.Label lblEmpleadoMenorSalario;
        private System.Windows.Forms.Label lblEmpleadoMayorSalario;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtIssE3;
        private System.Windows.Forms.TextBox txtSueldoE3;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtNombreE3;
        private System.Windows.Forms.TextBox txtRentaE3;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtAfpE3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtIssE2;
        private System.Windows.Forms.TextBox txtSueldoE2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtNombreE2;
        private System.Windows.Forms.TextBox txtRentaE2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtAfpE2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtIssE1;
        private System.Windows.Forms.TextBox txtSueldoE1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtNombreE1;
        private System.Windows.Forms.TextBox txtRentaE1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtAfpE1;
        private System.Windows.Forms.Label label1;
    }
}