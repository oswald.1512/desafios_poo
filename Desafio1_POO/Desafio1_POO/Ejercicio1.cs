﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Desafio1_POO
{
    public partial class frmEjercicio1 : Form
    {
        public frmEjercicio1()
        {
            InitializeComponent();
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            Double a = 0, b = 0, c = 0, x1 = 0, x2 = 0;
            if (txtA.Text.Length == 0 || txtB.Text.Length == 0 || txtC.Text.Length == 0)
            {
                MessageBox.Show("Debe ingresar todos los campos", "Alerta");

            }
            else
            {
                //Se leen los datos
                a = Convert.ToDouble(txtA.Text);
                b = Convert.ToDouble(txtB.Text);
                c = Convert.ToDouble(txtC.Text);


                if ((Math.Pow(b, 2) - (4 * a * c)) >= 0)
                {
                    x1 = (-b + Math.Sqrt((Math.Pow(b, 2) - (4.0 * a * c)))) / (2.0 * a);
                    x2 = (-b - Math.Sqrt((Math.Pow(b, 2) - (4.0 * a * c)))) / (2.0 * a);

                    lblRespuesta1.Text = x1.ToString();
                    lblRespuesta2.Text = x2.ToString();
                }
                else
                {
                    lblRespuesta1.Text = "( " + Convert.ToString(-b) + " + " + Convert.ToString(Math.Abs(Math.Sqrt(Math.Abs((Math.Pow(b, 2) - (4.0 * a * c)))))) + "i )/ " + Convert.ToString(2 * a);
                    lblRespuesta2.Text = "( " + Convert.ToString(-b) + " - " + Convert.ToString(Math.Abs(Math.Sqrt(Math.Abs((Math.Pow(b, 2) - (4.0 * a * c)))))) + "i )/ " + Convert.ToString(2 * a);
                }
            }
        }

        private void txtA_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Validando solo números y sus decimales
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '-') && (e.KeyChar != '.'))
            {
                MessageBox.Show("Solo se permiten números", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
                MessageBox.Show("No se permite más de un punto decimal", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
            }

            if ((e.KeyChar == '-') && ((sender as TextBox).Text.IndexOf('-') > -1))
            {
                e.Handled = true;
                MessageBox.Show("No se permite más de un guión", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
            }
        }

        private void txtB_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Validando solo números y sus decimales
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '-') && (e.KeyChar != '.'))
            {
                MessageBox.Show("Solo se permiten números", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
                MessageBox.Show("No se permite más de un punto decimal", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
            }

            if ((e.KeyChar == '-') && ((sender as TextBox).Text.IndexOf('-') > -1))
            {
                e.Handled = true;
                MessageBox.Show("No se permite más de un guión", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
            }
        }

        private void txtC_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Validando solo números y sus decimales
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '-') && (e.KeyChar != '.'))
            {
                MessageBox.Show("Solo se permiten números", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
                MessageBox.Show("No se permite más de un punto decimal", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
            }

            if ((e.KeyChar == '-') && ((sender as TextBox).Text.IndexOf('-') > -1))
            {
                e.Handled = true;
                MessageBox.Show("No se permite más de un guión", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
            }
        }
    }
}
