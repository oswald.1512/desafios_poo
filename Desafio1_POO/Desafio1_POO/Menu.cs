﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Desafio1_POO
{
    public partial class frmMenu : Form
    {
        public frmMenu()
        {
            InitializeComponent();
        }

        private void Ejercicio1_Click(object sender, EventArgs e)
        {
            frmEjercicio1 Ejercicio1 = new frmEjercicio1();
            Ejercicio1.Show();
        }

        private void Ejercicio2_Click(object sender, EventArgs e)
        {
            frmEjercicio2 Ejercicio2 = new frmEjercicio2();
            Ejercicio2.Show();
        }

        private void Ejercicio3_Click(object sender, EventArgs e)
        {
            frmEjercicio3 Ejercicio3 = new frmEjercicio3();
            Ejercicio3.Show();
        }

        private void Ejercicio4_Click(object sender, EventArgs e)
        {
            frmEjercicio4 Ejercicio4 = new frmEjercicio4();
            Ejercicio4.Show();
        }
    }
}
